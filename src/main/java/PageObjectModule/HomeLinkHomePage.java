package PageObjectModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;
import static org.junit.Assert.assertTrue;

public class HomeLinkHomePage {

    private static WebDriver driver;
    @FindBy(how= How.XPATH,xpath="//span[text()='More']")
    static WebElement MoreFilter;
    @FindBy(xpath="(//*[@id=\"bedrooms\"]//following::div)[1]")
    static WebElement addBedroom;
    @FindBy(xpath="//*[text()=\"Apply\"]")
    static WebElement applyFilters;
    @FindBy(css = "#bedrooms")
    static WebElement getAppliedBedRoomCount;
    @FindBy(xpath = "//*[@id=\"search-wrapper\"]/a/div/span/span[2]")
    static List<WebElement> filteredRooms;
    public static void initialRequest()
    {
        WebDriverManager.getInstance(CHROME).setup();
        DesiredCapabilities acceptSSlCertificate = DesiredCapabilities.chrome();
        acceptSSlCertificate.setCapability( CapabilityType.ACCEPT_SSL_CERTS, true );
        driver = new ChromeDriver( acceptSSlCertificate );
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
        PageFactory.initElements(driver, HomeLinkHomePage.class);
    }
    public static void launchApplication(String url){
        initialRequest();
        driver.get( url );
        assertTrue(MoreFilter.isDisplayed());
        MoreFilter.click();
    }
    public static void addBedRooms(int filterBedRooms){
        for(int i=1;i<=filterBedRooms;i++){
        addBedroom.click();
        if(i>4){
            Assert.fail( "cannot allow more than 4 bedrooms to filter" );
            }
        }
        applyFilters.click();
        MoreFilter.click();
    }
    public static void verifyTheFilteredRooms(){
        String filteredBedRoomsCount=getAppliedBedRoomCount.getAttribute( "value" );
        int convertAppliedBedRoomsCountToInteger=Integer.parseInt(filteredBedRoomsCount);
        for(int i=0;i<=filteredRooms.size()-1;i++){
            String getBedRoomsOfEachHome=filteredRooms.get( i ).getText();
            String result =getBedRoomsOfEachHome.substring(0, getBedRoomsOfEachHome.length() - 9);
            int eachHomeBedRoomCount=Integer.parseInt(result);
            if(eachHomeBedRoomCount>=convertAppliedBedRoomsCountToInteger) {
                Assert.assertTrue( "The filtered result list is displayed correctly ",true );
            }
            else{
                Assert.fail( "filter by bed rooms is not working" );
            }
        }

    }
    public static void closeTheBrowser(){
        driver.quit();
    }

}
