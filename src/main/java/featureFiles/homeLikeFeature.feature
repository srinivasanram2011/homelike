Feature:Automate HomeLike UI Application
  Description : To verify the filter Bed is filtered correctly
  Scenario: Verify the rooms are filtered correctly
    Given Open the URL
    When Click the More Button and apply filters
    And Verify the rooms are displayed by filters
    Then Close the browser
