package step_definitions;

import PageObjectModule.HomeLinkHomePage;
import cucumber.api.java8.En;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class HomeLikeDef implements En {
    String URL;
    public HomeLikeDef() throws IOException {
        FileReader reader=new FileReader("src/main/java/PageObjectModule/property.properties");
        Properties read=new Properties();
        read.load(reader);
        URL=read.getProperty("url");


        Given( "^Open the URL$", () -> {
            HomeLinkHomePage.launchApplication(URL);
        } );
        When( "^Click the More Button and apply filters$", () -> {
            HomeLinkHomePage.addBedRooms(3);
        } );
        And( "^Verify the rooms are displayed by filters$", () -> {
            HomeLinkHomePage.verifyTheFilteredRooms();
        } );
        Then( "^Close the browser$", () -> {
            HomeLinkHomePage.closeTheBrowser();
        } );
    }
}
