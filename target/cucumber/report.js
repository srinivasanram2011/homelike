$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/main/java/featureFiles/homeLikeFeature.feature");
formatter.feature({
  "name": "Automate HomeLike UI Application",
  "description": "  Description : To verify the filter Bed is filtered correctly",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Verify the rooms are filtered correctly",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open the URL",
  "keyword": "Given "
});
formatter.match({
  "location": "HomeLikeDef.java:20"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click the More Button and apply filters",
  "keyword": "When "
});
formatter.match({
  "location": "HomeLikeDef.java:23"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify the rooms are displayed by filters",
  "keyword": "And "
});
formatter.match({
  "location": "HomeLikeDef.java:26"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Close the browser",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeLikeDef.java:29"
});
formatter.result({
  "status": "passed"
});
});